x=float(input("Por favor ingrese el valor de X"))
y=float(input("Por favor ingrese el valor de Y"))
r=float(input("Por favor ingrese el valor del radio"))

if(r>0):
  print("+",end="")
print(r)

if x > 0 and y > 0:
  print("El punto se encuentra en el Primer Cuadrante del plano cartesiano")
if x < 0 and y > 0:
  print("El punto se encuentra en el Segundo Cuadrante del plano cartesiano")
if x < 0 and y < 0:
  print("El punto se encuentra en el Tercer Cuadrante del plano cartesiano")
if x > 0 and y < 0:
  print("El punto se encuentra en el  Cuarto Cuadrante del plano cartesiano")
if x == 0 and y == 0:
  print("El punto se encuentra en el  Origen del plano cartesiano")

if x == 0 or y == 0:
  if y != 0:
    print("El punto se encuentra en el eje Y")
  if x != 0:
    print("El punto se encuentra en el eje X")
  
if y > x:
  print("El punto se encuentra encima la recta Y=X")
if y < x:
  print("El punto se encuentra debajo la recta Y=X")
if y == x:
  print("El punto se encuentra en la recta Y=X")

num = int(input('digite un número: '))

if num % 2 == 0:
  print(num, 'es par') 
     #está en la suite que identifica que el residuo del número dado entre 2 es igual a cero
else:
    print(num, 'es impar') 

print(num, 'es par') if num % 2 == 0 else print(num, 'es impar') 